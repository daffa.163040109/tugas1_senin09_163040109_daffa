<?php 

/**
 * 
 */
class Daftar_komputer
{

	private $table = 'komputer';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllKomputer()
	{
		$this->db->query("SELECT * FROM " . $this->table);
		return $this->db->resultSet();
	}

	public function getKomputerById($id)
	{

		$this->db->query("SELECT * FROM " . $this->table . " WHERE kode=:id");
		$this->db->bind("id", $id);
		return $this->db->single();
	}
	public function insert($tabel,$data =[]){
		$this->table = $tabel;
		$this->db->query("insert into ".$this->table."(nama_perangkat,merk,harga,gambar) values(:nama_perangkat,:merk,:harga,:gambar)");
		$this->db->bind("nama_perangkat",$data["nama_perangkat"]);
		$this->db->bind("merk",$data["merk"]);
		$this->db->bind("harga",$data["harga"]);
		$this->db->bind("gambar",$data["gambar"]);
		$this->db->execute();
	}
	public function ubah($tabel, $data = []){
			$this->table = $tabel;
			$this->db->query("update ". $this->table ." set nama_perangkat=:nama_perangkat, merk=:merk, harga=:harga, gambar=:gambar  where kode=:kode");
			$this->db->bind("nama_perangkat", $data["nama_perangkat"]);
			$this->db->bind("merk", $data["merk"]);
			$this->db->bind("harga", $data["harga"]);
			$this->db->bind("kode", $data["kode"]);
			$this->db->bind("gambar",$data["gambar"]);
			$this->db->execute();
		}
	public function hapus($tabel, $id){
			$this->table = $tabel;
			$this->db->query("delete from ". $this->table .' where kode=:id');
			$this->db->bind("id",$id);
			$this->db->execute();
		}
	public function cari($tabel, $data){
			$this->table = $tabel;
			$this->db->query("SELECT * FROM ". $this->table ." WHERE kode=:cari OR nama_perangkat=:cari OR harga=:cari OR merk=:cari");
			$this->db->bind("cari",$data);
			return $this->db->resultSet();
		}
}