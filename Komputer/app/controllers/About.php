<?php  

/**
 * 
 */
class About extends Controller
{
	
	public function index($merk = "Intel", $nama_perangkat = "Prosessor", $nominal = "750000")
	{
		$data = array(
				 'buku' => $merk,
				 'oleh' => $nama_perangkat,
				 'nominal' => $nominal,
				 'judul' => 'About Me'
				);
		$this->view('templates/header', $data);
		$this->view('about/index', $data);
		$this->view('templates/footer');
	}

	public function page()
	{
		$data['judul'] = "Pages";
		$this->view('templates/header', $data);
		$this->view('about/page');
		$this->view('templates/footer');
	}
}