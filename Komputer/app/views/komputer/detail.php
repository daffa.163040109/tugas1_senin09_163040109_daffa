<div class="container mt-5 ">
	<div class="card" style="width: 18rem;">

		<div class="card-body">
			<h5 class="card-title"><?= $data['komputer']['merk'] ?></h5>
			<img src="<?= BASEURL; ?>/img/<?=$data['komputer']['gambar']  ?>">

			<?php echo "Nama Perangkat"; ?>
			<h4 class="card-subtitle mb-2 text-muted"><?= $data['komputer']['nama_perangkat'] ?></h4>
			<?php echo "Harga"; ?>	
			<h4 class="card-text">Rp. <?= $data['komputer']['harga']; ?></h4>
			<a href="<?= BASEURL ?>/komputer" class="card-link">Kembali</a>
		</div>
	</div>
</div>