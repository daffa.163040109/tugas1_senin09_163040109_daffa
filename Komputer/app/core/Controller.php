<?php

class Controller {

	public function controllers($controllers, $data = [])
	{
		require_once '../app/controllers/' . $controllers . '.php';
	}

	public function view($view, $data = [])
	{
		require_once '../app/views/' . $view . '.php';
	}

	public function model($model)
	{
		require_once '../app/models/' . $model . '.php';
		return new $model;
	}
}