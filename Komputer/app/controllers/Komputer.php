<?php 

/**
 * 
 */
class Komputer extends Controller
{
	public function index()
	{
		$data = array(
			'judul' => "Daftar Komputer",
			'komputer' => $this->model('Daftar_komputer')->getAllKomputer()
		);
		$this->view('templates/header', $data);
		$this->view('komputer/index', $data);
		$this->view('templates/footer');
	}

	public function detail($id)
	{
		$data = array(
			'judul' => "Daftar Komputer",
			'komputer' => $this->model('Daftar_komputer')->getKomputerById($id)
		);
		$this->view('templates/header', $data);
		$this->view('komputer/detail', $data);
		$this->view('templates/footer');
	}
	public function edit(){
			if(isset($_POST['ubah'])){
				$data['kode'] = $_POST["kode"];
				$data['nama_perangkat'] = $_POST["nama_perangkat"];
				$data['merk'] = $_POST['merk'];
				$data['harga'] = $_POST['harga'];
				$data['gambar']=$_POST['gambar'];
				$this->model('Daftar_komputer')->ubah("komputer", $data);
				$this->index();
			}
			
		}
	public function ubah($id){
			$data = array(
					'judul' => 'Daftar komputer ',
					'komputer' => $this->model('Daftar_komputer')->getKomputerById($id)
					
					);
			$this->view('templates/header', $data);
			$this->view('komputer/ubah',$data);
			$this->view('templates/footer');
		}

	public function hapus($id){
			$this->model('Daftar_komputer')->hapus("komputer", $id);
			$this->index();
		}
	public function insert(){
		if(isset($_POST['submit'])){
		$data['nama_perangkat']= $_POST['nama_perangkat'];
		$data['merk']= $_POST['merk'];
		$data['harga']= $_POST['harga'];
		$data['gambar']=$_POST['gambar'];
		$this->model('Daftar_komputer')->insert("komputer",$data);
		$this->index();
		}

	}
	public function cari(){
			$nama = $_POST["nama"];
			if(isset($_POST["cari"]))
			$data = array(
					'judul' => 'Daftar komputer',
					'komputer'=>$this->model('Daftar_komputer')->cari("komputer", $nama)
					);
			$this->view('templates/header', $data);
			$this->view('komputer/index',$data);
			$this->view('templates/footer');
		}
	
}