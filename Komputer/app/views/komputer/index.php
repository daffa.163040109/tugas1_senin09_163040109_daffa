	<div class="container mt-5">
	<div class="col-16"><h1>Daftar Perangkat KomputerKu</h1></div>

	<button type="button" class="btn btn-basic" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" style="margin-bottom: 20px;">Tambah</button>
	<div class="row">
		
			
			<?php foreach ($data['komputer'] as $key) : ?>
				<div class="col-3">
			<div class="card" style="width: 280px; margin-bottom: 30px;">
				<div class="card-body ">
					<img src="<?= BASEURL; ?>/img/<?=$key['gambar']  ?>">
					<h5 class="card-title"><?= $key['merk'] ?></h5>
					<h4 class="card-title">Rp. <?= $key['harga'] ?></h4>
				<a href="<?= BASEURL; ?>/komputer/detail/<?= $key['kode']; ?>" class="badge badge-primary">
				Detail</a>
				<a href="<?= BASEURL; ?>/komputer/ubah/<?= $key['kode']; ?>" class="badge badge-info" >Ubah</a>
				<a href="<?= BASEURL; ?>/komputer/hapus/<?= $key['kode']; ?>" class="badge badge-danger">Hapus</a>
				</td>
				</div>
			</div>

			</div>
			<?php endforeach; ?>
		</div>
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Hardware</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
				<form action="<?= BASEURL; ?>/Komputer/insert" method="post">
				  <div class="form-group">
					<label for="nama"  class="col-form-label">Nama Perangkat</label>
					<input type="text" name="nama_perangkat" id="nama" class="form-control">
				  </div>
				  <div class="form-group">
					<label for="merk" class="col-form-label">Merk</label>
					<input type="text" name="merk" id="merk"  class="form-control">
				  </div>
				  <div class="form-group">
					<label for="harga" class="col-form-label">Harga</label>
					<input type="text" name="harga" id="harga"  class="form-control">
				  </div>
				   <div class="form-group">
					<label for="gambar" class="col-form-label">Gambar</label>
					<input type="text" name="gambar" id="gambar"  class="form-control">
				  </div>
			  </div>
			  <div class="modal-footer">
				<button type="submit" name="submit" class="btn btn-primary">Save</button>
			  </div>
			 </form>
			</div>
		  </div>
		</div>
		
	</div>
</div>